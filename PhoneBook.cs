using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _08_30_2018_
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] num = { "077", "043", "041", "098", "091", "055", "093", "094", "095", "096" };
            var rand = new Random();
            int count = 20;
            Students[] students = new Students[count];
            for (int i = 0; i < count; i++)
            {
                students[i] = new Students($"student{i}");
            }
            DateTime dt = DateTime.Now;
            for (int i = 0; i < count; i++)
            {
                //students[i].phoneNumber += (num[rand.Next(0, 10)]);
                students[i].phoneNumber.Append((num[rand.Next(0, 10)]));
                for (int j = 0; j < 6; j++)
                {
                    // students[i].phoneNumber += (rand.Next(0, 9));
                    students[i].phoneNumber.Append(rand.Next(0, 9));
                }
            }
            DateTime dt1 = DateTime.Now;
            TimeSpan ts = dt1 - dt;
            Console.WriteLine(ts);
            Console.WriteLine();
            //for (int i = 0; i < count; i++)
            //{
            //    Console.WriteLine($"Name of student: {students[i].name}, phone number: {students[i].phoneNumber}, age: {students[i].Age}");
            //}
            //Console.WriteLine();

            //  Dictionary<string, Students> phoneBook = new Dictionary<string, Students>();
            Dictionary<StringBuilder, Students> phoneBook = new Dictionary<StringBuilder, Students>();

            //  phoneBook.Add(students[0].phoneNumber, students[0]);
            phoneBook.Add(students[0].phoneNumber, students[0]);

            for (int i = 0; i < count; i++)
            {
                AddContact(phoneBook, students[i]);
            }
            Print(phoneBook);
        }
        public static void Print(Dictionary<StringBuilder,Students> dict)
        {
            foreach (var item in dict)
            {
                Console.WriteLine($"Name: {item.Value.name}, phone number: {item.Key}");
            }
        }
        public static void AddContact(Dictionary<StringBuilder,Students> pb, Students student)
        {
            List<Students> repeatedPhoneNumbers = new List<Students>();
          //  string key = student.phoneNumber;
            StringBuilder key = student.phoneNumber;
            if (pb.ContainsKey(key))
            {
                repeatedPhoneNumbers.Add(student);
            }
            else
            {
                pb.Add(key,student);
            }
        }
    }
}
