using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _08_30_2018_
{
    class Students
    {
        public string name;
        private int age;
        public int Age
        {
            get { return age; }
            set
            {
                if (value < 0)
                    age = 0;
                else
                    age = value;
            }
        }
        //public string phoneNumber
        public StringBuilder phoneNumber=new StringBuilder("");
        public Students(string Name, int Age, StringBuilder PhoneNumber)
        {
            name = Name;
            this.Age = Age;
            phoneNumber = PhoneNumber;
        }
        public Students(string Name)
        {
            name = Name; 
        }
        public Students(string Name, int Age)
        {
            name = Name;
            this.Age = Age;
        }
        public Students()
        {
            name = "unknown student";
            Age = 0;
            phoneNumber.Append("0");
            //phoneNumber = "0";
        }
    }
}
